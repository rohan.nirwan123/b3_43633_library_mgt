#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "library.h"

void librarian_area(user_t *u) 
{
	int choice;
	char name[80];
	do {
		printf("\nLIBRARIAN MENU\n0.Sign Out\n1.Add member\n2.Edit Profile\n3.Change Password\n4.Add Book\n5.Find Book\n6.Edit Book\n7.Check Availability");
        printf("\n8.Add Copy\n9.Change Rack\n10.Issue Copy\n11.Return Copy\n12.Take Payment\n13.Payment History\n");
        printf("\nEnter your Choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1:add_member();
				   break;
			case 2:
				break;
			case 3:
				break;
			case 4:add_book();
				   break;
			case 5://find Book
				   printf("Enter Book Name :");
				   scanf("%s",name);
				   book_find_by_name(name);
				   break;
			case 6://Book Edit
				   book_edit_by_id();
				   break;
			case 7:
				break;
			case 8:
				break;
			case 9:
				break;
			case 10:
				break;
			case 11:
				break;
			case 12:
				break;
			case 13:
				break;
		}
	}while (choice != 0);		
}

void add_member() 
{
	// input librarian details
	user_t u;
	user_accept(&u);
	user_add(&u);
}

void add_book()
{
	FILE *fp;
	// input book details
	book_t b;
	book_accept(&b);
	b.id = get_next_book_id();
	// add book into the books file
	// open books file.
	fp = fopen(BOOK_DB, "ab");
	if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
	// append book to the file.
	fwrite(&b, sizeof(book_t), 1, fp);
	printf("book added in file.\n");
	// close books file.
	fclose(fp);

}


void book_edit_by_id()
{
	int id, found=0;
	FILE *fp;
	book_t b;

	printf("\nEnter Book ID to search and Edit:");
	scanf("%d",&id);

	fp = fopen(BOOK_DB, "rb+");
	if(fp==NULL)
	{
		perror("Cannot open the book file");
		exit(1);
	}

	while(fread(&b, sizeof(book_t), 1, fp) > 0)
	{
		if(id == b.id)
		{
			found = 1;
			break;
		}
	}

	if(found)
	{
		long size =sizeof(book_t);
		book_t nb;
		book_accept(&nb);
		nb.id = b.id;

		fseek(fp, -size, SEEK_CUR);
		fwrite(&nb, size, 1, fp);

		printf("\nBOOK UPDATED SUCCESSFULLY \n");
	}
	else 
		printf("\nBOOK NOT FUND ");


	fclose(fp);
}